<?php
/**
 * Offroad functions and definitions.
 */

/**
 * We need to check that required plugins are active and installed
 */
require_once get_template_directory() . '/just-theme-framework-checker.php';
if ( ! $_jtf_checker->check_requirements() ) {
	// terminate if titan plugin is not activated.
	return;
}

require_once get_template_directory() . '/inc/helpers.php';
require_once get_template_directory() . '/inc/hooks.php';
require_once get_template_directory() . '/inc/template-funcs.php';

/**
 * Wrap theme start code into a function to be able to use child theme.
 * this code will be executed inside child theme.
 */
if ( ! function_exists( 'offroad_theme_starter' ) ) {
	/**
	 * Theme launcher function
	 */
	function offroad_theme_starter() {
		new \JustCoded\ThemeFramework\Autoload( 'offroad\App', get_template_directory() . '/app' );

		new \offroad\App\Theme();
		new \offroad\App\Admin\ThemeSettings();
	}
}

/**
 * Finally run our Theme setup
 * and ThemeOptions setup
 */
offroad_theme_starter();

<?php \JustCoded\ThemeFramework\Web\View::footer_begin(); ?>
<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="site-info">
		<?php
		$copy = offroad\App\Admin\ThemeSettings::get( 'copyright_text' );
		echo esc_html( $copy );
		?>

		<span class="sep"> | </span>

		<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'offroad' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'offroad' ), 'WordPress' ); ?></a>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

=== offroad ===

Contributors: justcoded
Requires at least: 4.7
Tested up to: 4.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

An MVC theme called offroad.

== Description ==

Hi. I'm an MVC theme developed special for you!

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 0.1 - Oct 15 =
* Development start


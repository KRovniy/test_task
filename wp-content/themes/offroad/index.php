<?php
/**
 * Main template in WordPress theme.
 * It isn't used in MVC structure, however it's mandatory
 *
 * @see /views/ folder instead
 *
 * @package Offroad
 */

die( 'Silence is golden!' );

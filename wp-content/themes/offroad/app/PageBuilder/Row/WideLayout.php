<?php
namespace offroad\App\PageBuilder\Row;

/**
 * Class WideLayout
 *
 * @package offroad\App\PageBuilder\Row
 */
class WideLayout extends \JustCoded\ThemeFramework\SOPanels\Grid12RowLayout {
	/**
	 * ID
	 *
	 * @var string
	 */
	public static $ID = 'full-width';

	/**
	 * Display name
	 *
	 * @var string
	 */
	public static $TITLE = 'Full Width';

	/**
	 * Adjust html attributes for row wrapper div
	 *
	 * @param array $attributes container attributes.
	 * @param array $panel_data panel data settings.
	 *
	 * @return array    update attributes
	 */
	public function row_wrapper( $attributes, $panel_data ) {
		$attributes['class'] .= ' jpnl-wide-wrapper';

		return $attributes;
	}

	/**
	 * Adjust html attributes for row div
	 *
	 * @param array $attributes container attributes.
	 * @param array $style_data row settings.
	 *
	 * @return array    update attributes
	 */
	public function row( $attributes, $style_data ) {
		$attributes['class'][] = ' jpnl-wide';

		return $attributes;
	}
}

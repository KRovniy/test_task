<?php
namespace offroad\App\PageBuilder;

/**
 * Class SiteOriginPanelsLoader
 * Register SiteOrigin Panels special layouts, which can define classes and other html attributes for
 * row/cell wrappers.
 *
 * @package offroad\App\PageBuilder
 */
class SiteOriginPanelsLoader extends \JustCoded\ThemeFramework\SOPanels\SiteOriginPanelsLoader {

	/**
	 * Default RowLayout to be loaded if it's not specified by Row settings manually
	 *
	 * @var string
	 */
	public $default_row_layout = '\JustCoded\ThemeFramework\SOPanels\Grid12RowLayout';

	/**
	 * Default namespace of rows/widgets if relative name specified
	 *
	 * @var string
	 */
	public $default_layout_namespace = '\offroad\App\PageBuilder';

	/**
	 * Init row/widgets layouts and change disabled plugins list.
	 * Called at the end of the __contruct() method
	 *
	 * To add row layout please call:
	 * $this->register_row_layout( 'LayoutClassName' );
	 *        OR
	 * $this->register_widget_layout( 'LayoutClassName' );
	 *
	 *
	 * To enable back some plugins please do the following:
	 * unset(array_search('Widget_Class_name', $this->disabled_wordpress_widgets));
	 *        OR
	 * unset(array_search('Widget_Class_name', $this->disabled_siteorigin_widgets));
	 */
	public function init() {
		parent::init();

		$this->register_row_layout( 'Row\WideLayout' );

		$this->register_widget_layout( 'Widget\HeroLayout' );
	}
}

<?php
namespace offroad\App\PageBuilder\Widget;

/**
 * Class HeroLayout
 *
 * @package offroad\App\PageBuilder\Widget
 */
class HeroLayout extends \JustCoded\ThemeFramework\SOPanels\WidgetLayout {
	/**
	 * ID
	 *
	 * @var string
	 */
	public static $ID = 'hero-widget';

	/**
	 * Display name
	 *
	 * @var string
	 */
	public static $TITLE = 'Hero size';

	/**
	 * Replace wrapper classes string
	 *
	 * @param array  $classes html attribute.
	 * @param array  $style_data selected style options.
	 * @param string $widget widget class name.
	 * @param array  $instance widget data.
	 *
	 * @return array
	 */
	public function wrapper_classes( array $classes, $style_data, $widget, $instance ) {
		$classes[] = ' jpnl-widget-hero';

		return $classes;
	}

	/**
	 * Adjust html attributes for widget div
	 *
	 * @param array $attributes  container attributes.
	 * @param array $panel_data  panel data settings.
	 *
	 * @return array    update attributes
	 */
	public function widget( $attributes, $panel_data ) {
		return $attributes;
	}

}

<?php
/**
 * Example of custom taxonomy and it's archive
 * Delete if you don't have any
 *
 * @package   Offroad
 */

namespace offroad\App\Taxonomy;

use JustCoded\ThemeFramework\Objects\Taxonomy;

/**
 * Class Example Taxonomy
 *
 * @package offroad\App\Taxonomy
 */
class Example extends Taxonomy {
	/**
	 * ID
	 *
	 * @var string
	 */
	public static $ID = 'example';

	/**
	 * Rewrite URL part
	 *
	 * @var string
	 */
	public static $SLUG = 'tax-example';

	/**
	 * Registration function
	 */
	public function init() {
		$this->label_singular = 'Some Taxo';
		$this->label_multiple = 'Some Taxo\'s';
		$this->textdomain     = 'offroad';

		$this->is_hierarchical  = false;
		$this->has_single       = true;
		$this->rewrite_singular = false;

		$this->has_admin_menu = true;

		$this->post_types = array(
			\offroad\App\PostType\Example::$ID,
		);

		$this->register();
	}
}

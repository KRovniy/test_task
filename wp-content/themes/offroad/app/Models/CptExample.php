<?php
namespace offroad\App\Models;

use JustCoded\ThemeFramework\Objects\Model;
use offroad\App\PostType;

/**
 * Example of getting data for archive pages of custom post type
 *
 * @property \WP_Query $query
 */
class CptExample extends Model {
	/**
	 * Get hero query to be used in home views in the loop
	 *
	 * @return \WP_Query  query object to be used in loop
	 */
	public function get_query() {
		return $this->archive_query( array(
			'post_type'      => PostType\Example::$ID,
			'post_status'    => PostType\Example::STATUS_PUBLISH,
			'order'          => PostType\Example::SORT_DESC,
			'orderby'        => PostType\Example::ORDERBY_DATE,
			'posts_per_page' => 2,
		), __METHOD__ );
	}

}
